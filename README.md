   Loan Provision System (Spring Boot)


   Requirements
        -Maven Plugin
        -JDK 17
        -MySQL 8.4


   Setup Instructions
   
                1. Install JDK 17
                       Ensure JDK 17 is installed on your machine. You can download it from  Oracle JDK.

                2. Install MySQL 8.4
                        Install MySQL 8.4 database server. You can download it from MySQL Downloads.

                3. Clone the Repository

                        git clone https://gitlab.com/miki221990/loan_provission_exam.git
                        cd loan_provission_exam

                4.Configure MySQL Connection
                    Modify application-dev.yml file in src/main/resources to configure MySQL connection properties:
                    spring.datasource.url=jdbc:mysql://localhost:3306/loan
                    spring.datasource.username=yourusername
                    spring.datasource.password=yourpassword

                5. Import MySQL Database Dump
                    Use the provided SQL dump to initialize the database

                         root directory  ./ dump-loan-202406181650
                    
                    or you can run create script using sql file

                          root directory ./ loan.sql

                6. Build and Run the Application

                    Build with Maven

           cmd             ./mvnw clean install
                    
                    Run with Maven

           cmd             mvn spring-boot:run

             
                 7. Access Swagger UI
                    
                    Open your web browser and navigate to

                    http://localhost:9191/swagger-ui/index.html



