package com.example.loan.bank;


public class MockBank {

    private String accountNumber;
    private double balance;

    public MockBank(String accountNumber, double initialBalance) {
        this.accountNumber = accountNumber;
        this.balance = initialBalance;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public double getBalance() {
        return balance;
    }

    public void deposit(double amount) {
        if (amount <= 0) {
            throw new IllegalArgumentException("Deposit amount must be positive.");
        }
        this.balance += amount;
        System.out.println("Deposited " + amount + ". New balance: " + this.balance);
    }

    public Boolean transfer(MockBank targetAccount, double amount) {
        if (amount <= 0) {
            throw new IllegalArgumentException("Transfer amount must be positive.");
        }
        if (this.balance < amount) {
            throw new IllegalArgumentException("Insufficient balance for transfer.");
        }
        this.balance -= amount;
        targetAccount.deposit(amount);
        System.out.println("Transferred " + amount + " to account " + targetAccount.getAccountNumber() + ". New balance: " + this.balance);
        return true;
      
    }

    @Override
    public String toString() {
        return "MockBank{" +
                "accountNumber='" + accountNumber + '\'' +
                ", balance=" + balance +
                '}';
    }
}
