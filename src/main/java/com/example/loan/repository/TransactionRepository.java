package com.example.loan.repository;

import com.example.loan.entity.Loan;
import com.example.loan.entity.Repayment;
import com.example.loan.entity.Transaction;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;


public interface TransactionRepository extends JpaRepository<Transaction,Integer> {
    
}
