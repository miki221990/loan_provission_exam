package com.example.loan.repository;


import com.example.loan.entity.Loan;
import com.example.loan.entity.Repayment;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface RepaymentRepository extends JpaRepository<Repayment,Integer> {
    @Query("SELECT SUM(r.amountPaid) FROM Repayment r WHERE r.loan.id = :loanId")
    Double sumTotalAmountPaidByLoanId(Integer loanId);
    @Modifying
    @Query(value = "UPDATE repayment SET outstanding_amount = :outstandingBalance WHERE id = :id" , nativeQuery = true)
    int updateOutStandingById(@Param("id") Integer id, @Param("outstandingBalance") Double outstandingBalance);
    
    // @Query("SELECT r FROM Repayment r WHERE r.loan.id = :loanId")
    // Page<Repayment> findAllByLoanId(@Param("loanId") Integer loanId, Pageable pageable);
    @Query(value = "SELECT * FROM repayment r WHERE r.loan_id = :loanId", nativeQuery = true)
    List<Repayment> findAllByLoanId(@Param("loanId") Integer loanId);

    @Query(value = "SELECT * FROM repayment r WHERE r.loan_id = :loanId ORDER BY r.id DESC LIMIT 1", nativeQuery = true)
    Repayment findLatestRepaymentByLoanId(@Param("loanId") Integer loanId);

}
