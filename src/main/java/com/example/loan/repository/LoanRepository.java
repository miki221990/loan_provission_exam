package com.example.loan.repository;

import com.example.loan.entity.Loan;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface LoanRepository extends JpaRepository<Loan,Integer> {

    @Modifying
    @Query(value = "UPDATE loan SET status_id = :statusId WHERE id = :id" , nativeQuery = true)
    int updateStatusById(@Param("id") Integer id, @Param("statusId") Integer statusId);
    
}
