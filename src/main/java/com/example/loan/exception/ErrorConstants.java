package com.example.loan.exception;


public final class ErrorConstants {

    public static final String ERR_CONCURRENCY_FAILURE = "error.concurrencyFailure";
    public static final String ERR_VALIDATION = "error.validation";
    public static final String DEFAULT_TYPE = "problem-with-message";
    public static final String CONSTRAINT_VIOLATION_TYPE = "constraint-violation";


    private ErrorConstants() {}
}