package com.example.loan.service.dto;

import java.sql.Timestamp;

import com.example.loan.entity.DictionaryItem;
import com.example.loan.entity.Loan;

import jakarta.persistence.Column;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToOne;
import lombok.Data;

@Data
public class RepaymentDTO {
   
    private Integer id;

    private Loan loan;

   
    private TransactionDTO transaction;

   
    private Double amountPaid;


    private Double outstandingAmount;


    private Double interestPaid;

    private DictionaryItem status;

    private Timestamp created_at;
}
