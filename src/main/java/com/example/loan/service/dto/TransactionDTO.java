package com.example.loan.service.dto;

import java.sql.Timestamp;

import com.example.loan.entity.DictionaryItem;
import com.example.loan.entity.Loan;

import jakarta.persistence.Column;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import lombok.Data;

@Data
public class TransactionDTO {

   
    private Integer id;

    private Double amount;


    private String accountNumber;

    private LoanDTO loan;

 
    private String reason;

    private DictionaryItemDTO status;


    private Timestamp created_at;
    
}
