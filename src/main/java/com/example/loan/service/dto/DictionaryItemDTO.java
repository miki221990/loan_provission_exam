package com.example.loan.service.dto;

import java.io.Serializable;

import jakarta.persistence.Column;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import lombok.Data;

@Data
public class DictionaryItemDTO implements Serializable {

    private Integer id;

  
    private String name;

  
    private DictionaryDTO dictionary;
}
