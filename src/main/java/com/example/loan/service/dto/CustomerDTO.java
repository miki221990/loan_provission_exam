package com.example.loan.service.dto;

import java.io.Serializable;
import java.sql.Timestamp;

import jakarta.validation.constraints.Digits;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import lombok.Data;


@Data
public class CustomerDTO implements Serializable{
     
    private Integer id;
    @NotEmpty(message = "username cannot be empty")
    private String username;

    @NotNull(message = "salary cannot be null")
    @Digits(integer = 10, fraction = 5, message = "Amount must be a valid number with up to 5 decimal places")
    private Double salary;

    @NotEmpty(message = "account number cannot be empty")
    private String accountNo;

    @NotEmpty(message = "phone number cannot be empty")
    private String phoneNumber;
    private Timestamp created_at;
}
