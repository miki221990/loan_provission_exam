package com.example.loan.service.dto;

import java.io.Serializable;
import java.sql.Timestamp;

import com.example.loan.entity.DictionaryItem;
import com.fasterxml.jackson.annotation.JsonFilter;

import jakarta.validation.constraints.Digits;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Pattern;
import jakarta.validation.constraints.Size;
import lombok.Data;

@Data
public class LoanDTO implements Serializable{
    private Integer id;

    @NotNull(message = "Amount cannot be null")
    @Digits(integer = 10, fraction = 5, message = "Amount must be a valid number with up to 5 decimal places")
    private Double amount;

    @NotNull(message = "customer cannot be empty")
    private CustomerDTO customer;

    @NotEmpty(message = "purpose cannot be empty")
    private String purpose;

    @NotNull(message = "Term cannot be null")
    @Digits(integer = 10, fraction = 5, message = "Amount must be a valid number with up to 5 decimal places")
    private Double term;

    @NotNull(message = "Interest cannot be null")
    @Digits(integer = 10, fraction = 5, message = "Amount must be a valid number with up to 5 decimal places")
    private Double interest;

    @NotNull(message = "Duration cannot be null")
    @Digits(integer = 10, fraction = 5, message = "Amount must be a valid number with up to 5 decimal places")
    private Double duration;

    private DictionaryItemDTO status;

    private Timestamp created_at;
    
}
