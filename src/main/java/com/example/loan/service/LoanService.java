package com.example.loan.service;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.List;
import java.util.Optional;

import org.hibernate.TransactionException;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.example.loan.bank.MockBank;
import com.example.loan.entity.DictionaryItem;
import com.example.loan.entity.Loan;
import com.example.loan.entity.Repayment;
import com.example.loan.entity.Transaction;
import com.example.loan.exception.ResourceNotFoundException;
import com.example.loan.exception.ServiceException;
import com.example.loan.repository.LoanRepository;
import com.example.loan.repository.RepaymentRepository;
import com.example.loan.repository.TransactionRepository;
import com.example.loan.service.dto.LoanDTO;
import com.example.loan.service.dto.RepaymentDTO;
import com.example.loan.service.dto.TransactionDTO;
import com.example.loan.service.tools.MakeTransaction;
import com.example.loan.service.tools.RepaymentHandle;
import com.example.loan.service.tools.ValidateLoan;
import com.example.loan.service.tools.ValidationResponse;

import jakarta.persistence.EntityNotFoundException;
import jakarta.transaction.Transactional;

@Service
public class LoanService {
	 private final Logger log = LoggerFactory.getLogger(LoanService.class);

	  @Autowired
	 private LoanRepository loanRepository;

     @Autowired
     private TransactionRepository transactionRepository;

     @Autowired
     private RepaymentRepository repaymentRepository;


	@Autowired
	  private ModelMapper modelMapper;


      @Transactional
	public LoanDTO saveLoan(LoanDTO loanRequest) {
		log.debug("Request to create loan : {}", loanRequest);
		try {
            Loan loan = this.modelMapper.map(loanRequest, Loan.class);
            Loan savedLoan = loanRepository.save(loan);
            return this.modelMapper.map(savedLoan, LoanDTO.class);
        } catch (Exception e) {
            log.error("Error occurred while saving : {}", e.getMessage(), e);
            throw new ServiceException("Failed to save service", e);
        }

	}

	@Transactional
    public Page<LoanDTO> getAllLoans(Pageable pageable) {
        log.debug("Request to get all Loan Application");
		try {
            Page<Loan> loans = loanRepository.findAll(pageable);
            return loans.map(loan -> modelMapper.map(loan, LoanDTO.class));
        } catch (Exception e) {
        
            e.printStackTrace();
            throw new ResourceNotFoundException("Failed to fetch loans: " + e.getMessage());
        }

    }

    @Transactional
    public List<Repayment> getRepaymentByloanId(Pageable pageable,Integer loanId) {
        log.debug("Request to get all Repayments by loan id");
		try {
            List<Repayment> repayments = repaymentRepository.findAllByLoanId(loanId);
           
            return repayments;
        } catch (Exception e) {
        
            e.printStackTrace();
            throw new ResourceNotFoundException("Failed to fetch loans: " + e.getMessage());
        }

    }

    @Transactional
    public LoanDTO getLoan(Integer id) {
        log.debug("Request to get Loan Application with id {}",id);
		try {
            Loan loan = loanRepository.findById(id).orElseThrow(() -> new EntityNotFoundException("loan application with id " + id + " not found"));;
            return this.modelMapper.map(loan, LoanDTO.class);
          
        } catch (Exception e) {
        
            e.printStackTrace();
            throw new ResourceNotFoundException("Failed to fetch loan: " + e.getMessage());
        }

    }

    
    @Transactional
    public LoanDTO approveLoan(Integer id) throws LoanValidationException{
        log.debug("Request to approve loan with id {}",id);
		try {
            Loan loan = loanRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("Loan Application with id " + id + " not found"));
                ValidateLoan loanValidator=new ValidateLoan();
                MockBank mockBank=new MockBank(loan.getCustomer().getAccountNo(),100000);
                ValidationResponse resp=loanValidator.isLoanValid(loan, mockBank);
                if(resp.isValid()){
                    Integer statusId=1002;
                    int result = loanRepository.updateStatusById(id, statusId);
                  
                }else if(!resp.isValid()){
                   
                    throw new LoanValidationException(resp.getReason());
                }
      
                return this.modelMapper.map(loan, LoanDTO.class);
          
            } catch (LoanValidationException exc) {
                throw  exc; 
            } catch (Exception e) {
                e.printStackTrace();
                throw new RuntimeException("Failed to update loan: " + e.getMessage());
            }

    }
    @Transactional
    public LoanDTO rejectLoan(Integer id) {
        log.debug("Request to reject loan with id {}",id);
		try {
            Loan loan = loanRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("Loan Application with id " + id + " not found"));
        Integer statusId=1003;
        int result = loanRepository.updateStatusById(id, statusId);
          return this.modelMapper.map(loan, LoanDTO.class);
        } catch (Exception e) {
        
            e.printStackTrace();
            throw new RuntimeException("Failed to update loan: " + e.getMessage());
        }

    }

    @Transactional
    public TransactionDTO disburse(Integer id) throws LoanValidationException {
        log.debug("Request to disburse loan to requester by loan id {}",id);
		try {
            Transaction result =new Transaction() ;
            Loan loan = loanRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("Loan Application with id " + id + " not found"));
             if(loan.getStatus().getId()==1002){
                MakeTransaction trans=new MakeTransaction();
                MockBank banktrans= new MockBank(loan.getCustomer().getAccountNo(), 100000);

                result=trans.makeLoanTransaction(loan, loan.getAmount(),banktrans );
                transactionRepository.save(result);
                if(result.getStatus().getId()==2002){
                    int res = loanRepository.updateStatusById(loan.getId(), 1004);

                }
                 
             }else{
                throw new LoanValidationException("Loan Application with id " + id + " cant be disbursed");
             }
             return  this.modelMapper.map(result, TransactionDTO.class);
        }
        catch (LoanValidationException exc) {
            throw  exc; 
        }
         catch (Exception e) {
        
            e.printStackTrace();
            throw new LoanValidationException("Failed to make transaction: " + e.getMessage());
        }

    }

    @Transactional
    public LoanDTO repayLoan(Integer loanId, Double amountToPay) throws TransactionException {
      try{
        Loan loan = loanRepository.findById(loanId)
        .orElseThrow(() -> new EntityNotFoundException("Loan not found with id: " + loanId));
         Repayment repayStatus=repaymentRepository.findLatestRepaymentByLoanId(loan.getId());
         if(repayStatus!=null && (repayStatus.getOutstandingAmount()==0.0)){
            throw new TransactionException("loan debt completed");
         }
        Transaction tran=new Transaction();
        if(loan.getStatus().getId()!=1004){
            throw new TransactionException("loan is not active to pay debt");
        }
        Double monthlyInterestRate = (loan.getInterest()/100)  / 12;

        Double interestTotal = loan.getAmount() * (loan.getInterest()/100) * (loan.getDuration() / loan.getTerm());

        Double monthlyInterest = loan.getAmount() * monthlyInterestRate * (loan.getDuration() / 12);
        log.debug("in sechhdde {}", monthlyInterest);
        
        if (amountToPay >= loan.getAmount()) {
             boolean payBefore=false;
            if(repayStatus!=null){
                amountToPay=repayStatus.getOutstandingAmount()+interestTotal;
                payBefore=true;
            }
         
            tran=handleOneTimePayment(loan, amountToPay, interestTotal,payBefore);
            Repayment repayment=saveRepaymentRecord(loan, amountToPay, interestTotal,tran);
            Double outstandingBalance = calculateOutstandingBalance(loanId);
            int res = loanRepository.updateStatusById(loan.getId(), 1005);
            repaymentRepository.updateOutStandingById(repayment.getId(),outstandingBalance );
        } else {
           
            tran=handleScheduledPayment(loan, amountToPay, monthlyInterest);
            Repayment repayment=saveRepaymentRecord(loan, amountToPay, monthlyInterest,tran);
            Double outstandingBalance = calculateOutstandingBalance(loanId);
            if(outstandingBalance==0.0){
                int res = loanRepository.updateStatusById(loan.getId(), 1005);
            }
            repaymentRepository.updateOutStandingById(repayment.getId(),outstandingBalance );
        }

      
       
        return  this.modelMapper.map(loan, LoanDTO.class);

    } catch (TransactionException exc) {
        throw  exc; 
    } catch (Exception e) {
        e.printStackTrace();
        throw new TransactionException("Failed to update loan: " + e.getMessage());
    }

           
        
    }




    private Repayment saveRepaymentRecord(Loan loan, Double amountToPay, Double interest,Transaction tran) {
        Repayment repayment = new Repayment();
        repayment.setLoan(loan);
        repayment.setInterestPaid(interest);
        repayment.setTransaction(tran);
        repayment.setAmountPaid(amountToPay + interest);
        repayment.setCreated_at(new Timestamp(System.currentTimeMillis()));
        DictionaryItem dict=new DictionaryItem();
        dict.setId(1002);
        repayment.setStatus(dict);
        Repayment repayResp=repaymentRepository.save(repayment);
        return repayResp;
    }

    public Transaction handleOneTimePayment(Loan loan, Double amountToPay, Double interest,boolean payBefore) throws TransactionException {
        Double totalAmountToPay=0.0;
        if(payBefore){
            totalAmountToPay =amountToPay;
        }else{
             totalAmountToPay = loan.getAmount() + interest;
        }
       
         Transaction transResponse =new Transaction();
      
        if (amountToPay < totalAmountToPay) {
            throw new IllegalArgumentException("Amount to pay is less than total amount due.");
        }

        MockBank bank=new MockBank(loan.getCustomer().getAccountNo(), 100000);
        boolean success = bank.transfer(bank, amountToPay);
        if(success){
          
            Transaction transact=new Transaction();
            transact.setLoan(loan);
            DictionaryItem repayDict=new DictionaryItem();
            repayDict.setId(2002);
            transact.setStatus(repayDict);
            transact.setAmount(totalAmountToPay);
            transact.setReason("Repay");

            transResponse= transactionRepository.save(transact);

        }
        if (!success) {
            throw new TransactionException("Bank transaction failed.");
        }
        return transResponse;
    }

    public Transaction handleScheduledPayment(Loan loan, Double amountToPay, Double interest) throws TransactionException {
        Transaction transResponse =new Transaction();
        Double monthlyRepayment = amountToPay;
        if (monthlyRepayment > (loan.getCustomer().getSalary() / 3)) {
            throw new IllegalArgumentException("Monthly repayment exceeds 1/3 of customer's salary.");
        }
        Double totalAmountToPay = amountToPay + interest;

   
       
        MockBank bank=new MockBank(loan.getCustomer().getAccountNo(), 100000);
        boolean success = bank.transfer(bank, totalAmountToPay);
        if(success){
          
            Transaction transact=new Transaction();
            transact.setLoan(loan);
            DictionaryItem repayDict=new DictionaryItem();
            repayDict.setId(2002);
            transact.setAmount(totalAmountToPay);
            transact.setStatus(repayDict);
            transact.setReason("Repay");

            transResponse= transactionRepository.save(transact);

        }
        if (!success) {
            throw new TransactionException("Bank transaction failed.");
        }
        return transResponse;
    }

    @Transactional
    public Double calculateOutstandingBalance(Integer loanId) {
        Double  outstandingBalance;
        Loan loan = loanRepository.findById(loanId)
                .orElseThrow(() -> new EntityNotFoundException("Loan not found with id: " + loanId));

        Double totalPaid = repaymentRepository.sumTotalAmountPaidByLoanId(loanId);
        if(totalPaid>loan.getAmount()){
            outstandingBalance= 0.0;
            return outstandingBalance;
        }
        outstandingBalance= loan.getAmount() - totalPaid;
       
       

        return outstandingBalance;
    }

    public class LoanValidationException extends Exception {
        public LoanValidationException(String message) {
            super(message);
        }
    }

}

