package com.example.loan.service.tools;

import lombok.Data;

@Data
public class ValidationResponse {
            private String reason;
            private boolean valid;
}
