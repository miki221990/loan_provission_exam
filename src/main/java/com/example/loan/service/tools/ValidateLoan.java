package com.example.loan.service.tools;

import com.example.loan.bank.MockBank;
import com.example.loan.entity.Customer;
import com.example.loan.entity.Loan;

public class ValidateLoan {

    public ValidationResponse isLoanValid(Loan loan, MockBank mockBank) {
        Customer customer = loan.getCustomer();
        double loanAmount = loan.getAmount();
        double salary = customer.getSalary();
        double duration = loan.getDuration();
        double balance = mockBank.getBalance();

        ValidationResponse valResp=new ValidationResponse();

        if (balance < (loanAmount / 4)) {
            System.out.println("Insufficient balance: Customer does not have at least 1/4 of the requested loan amount.");
            ValidationResponse resp=new ValidationResponse();
            resp.setValid(false);
            resp.setReason("Insufficient balance: Customer does not have at least 1/4 of the requested loan amount.");
            return resp;
        }

     
        double monthlyPayment = loanAmount / duration;
        if (monthlyPayment > (salary / 3)) {
            ValidationResponse resp=new ValidationResponse();
            resp.setValid(false);
            resp.setReason("Insufficient salary: Customer cannot pay off the loan by paying 1/3 of their salary each month within the specified duration.");
            return resp;
        }

        System.out.println("Loan is valid.");
        ValidationResponse resp=new ValidationResponse();
        resp.setReason("Loan is valid.");
        resp.setValid(true);
        return resp;
    }
}
 