package com.example.loan.service.tools;

import java.sql.Timestamp;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.loan.bank.MockBank;
import com.example.loan.entity.DictionaryItem;
import com.example.loan.entity.Loan;
import com.example.loan.entity.Transaction;

import com.example.loan.service.LoanService;

import jakarta.transaction.Transactional;

@Service
public class MakeTransaction {

 
     private final Logger log = LoggerFactory.getLogger(MakeTransaction.class);

    @Transactional
    public Transaction makeLoanTransaction(Loan loan, Double amount, MockBank mockBank) {
        try{
      
        boolean success = mockBank.transfer(mockBank, amount);


        Transaction transaction = new Transaction();
        transaction.setAmount(amount);
        transaction.setAccountNumber(loan.getCustomer().getAccountNo());
        transaction.setLoan(loan);
    
        transaction.setReason("Disburse");
        DictionaryItem statusDict=new DictionaryItem();
        statusDict.setId(2001);
        transaction.setStatus(statusDict); 
        transaction.setCreated_at(new Timestamp(System.currentTimeMillis()));

        if (success) {
        
            DictionaryItem statusDictSuccuss=new DictionaryItem();
            statusDictSuccuss.setId(2002);
            transaction.setStatus(statusDictSuccuss);
        } else {
            DictionaryItem statusDictFail=new DictionaryItem();
            statusDictFail.setId(2003);
       
            transaction.setStatus(statusDictFail);
        }
        log.debug("trannsaccttt", transaction);

       
        return transaction;
    } catch (Exception e) {
      
        throw new RuntimeException("Failed to make loan transaction: " + e.getMessage(), e);
    }
    }
   
}
