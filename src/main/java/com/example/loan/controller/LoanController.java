package com.example.loan.controller;

import java.net.URI;
import java.net.URISyntaxException;
import java.sql.Timestamp;
import java.time.Instant;
import java.time.LocalDateTime;
import java.util.List;

import org.hibernate.TransactionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

import com.example.loan.controller.request.RepaymentRequest;
import com.example.loan.entity.DictionaryItem;
import com.example.loan.entity.Loan;
import com.example.loan.entity.Repayment;
import com.example.loan.exception.ResourceNotFoundException;
import com.example.loan.service.LoanService;
import com.example.loan.service.LoanService.LoanValidationException;
import com.example.loan.service.dto.DictionaryItemDTO;
import com.example.loan.service.dto.LoanDTO;
import com.example.loan.service.dto.RepaymentDTO;
import com.example.loan.service.dto.TransactionDTO;

import jakarta.persistence.EntityNotFoundException;
import jakarta.validation.Valid;

import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;


@RestController
@RequestMapping("/api/loans")
@Validated
public class LoanController {
	
    private final Logger log = LoggerFactory.getLogger(LoanController.class);

	@Autowired
	private LoanService loanService;
	
	@PostMapping("")
	public ResponseEntity<LoanDTO> applyLoan(@Valid @RequestBody  LoanDTO loanDTO)throws URISyntaxException {
		log.debug("REST request to save loan: {}", loanDTO);
		LocalDateTime currentDateTime = LocalDateTime.now();
        Timestamp timestamp = Timestamp.valueOf(currentDateTime);
		loanDTO.setCreated_at(timestamp);
		DictionaryItemDTO dictLoan=new DictionaryItemDTO();
		dictLoan.setId(1001);
		loanDTO.setStatus(dictLoan);
		LoanDTO result=loanService.saveLoan(loanDTO);
	
	return ResponseEntity
            .created(new URI("/api/loans"))
            .body(result);
	}

	@GetMapping("")
	public ResponseEntity<Page<LoanDTO>> getAllLoanApplication(Pageable pageable) {
	
		try {
			Page<LoanDTO> loans=loanService.getAllLoans(pageable);
			return ResponseEntity.ok(loans);
        } catch (EntityNotFoundException e) {
            throw new ResourceNotFoundException("Not Found: " + e.getMessage());
        }
	
	}

	@GetMapping("/{id}")
	public ResponseEntity<LoanDTO> getLoanApplication(@PathVariable Integer id) {  
		
		try {
            LoanDTO loan=loanService.getLoan(id);
			return ResponseEntity.ok(loan);
        } catch (EntityNotFoundException e) {
			throw new ResourceNotFoundException("Not Found: " + e.getMessage());
        }
	}
	
	

	@PutMapping("/{id}/approve")
	public ResponseEntity<?> approveLoanApplication(@PathVariable Integer id) {  
		try {
            LoanDTO approvedLoan = loanService.approveLoan(id);
            return ResponseEntity.ok().body(approvedLoan);
         } catch (LoanValidationException e) {
        return ResponseEntity.badRequest().body(e.getMessage()); 
    }
		catch (EntityNotFoundException e) {
			throw new ResourceNotFoundException("Not Found: " + e.getMessage());
        }
	}

	
	@PutMapping("/{id}/reject")
	public ResponseEntity<LoanDTO> rejectLoanApplication(@PathVariable Integer id) {  
		try {
            LoanDTO rejectedLoan = loanService.rejectLoan(id);
            return ResponseEntity.ok().build();
        } catch (EntityNotFoundException e) {
			throw new ResourceNotFoundException("Not Found: " + e.getMessage());
        }
	}

	@PostMapping("/{id}/disburse")
	public ResponseEntity<?> disburseLoanApplication(@PathVariable Integer id) {  
		try {
            TransactionDTO disbursedLoan = loanService.disburse(id);
            return ResponseEntity.ok().body(disbursedLoan);
        }catch (LoanValidationException e) {
			return ResponseEntity.badRequest().body(e.getMessage()); 
		}
		 catch (EntityNotFoundException e) {
			throw new ResourceNotFoundException("Not Found: " + e.getMessage());
        }
	}
	@PostMapping("/{id}/repay")
	public ResponseEntity<?> repayLoanApplication(@PathVariable Integer id,@RequestBody RepaymentRequest repaymentRequest) throws TransactionException{  
		try {
			
            LoanDTO paidLoan = loanService.repayLoan(id,repaymentRequest.getAmount());
            return ResponseEntity.ok().body(paidLoan);
        } catch (IllegalArgumentException e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        } catch (TransactionException e) {
            return ResponseEntity.status(500).body("Transaction error: " + e.getMessage());
        } catch (EntityNotFoundException e) {
            return ResponseEntity.status(404).body("Not Found: " + e.getMessage());
        }
	}

	@GetMapping("/{loanId}/repayment_history")
	public ResponseEntity<List<Repayment>> getRepaymentHistory(@PathVariable Integer loanId,Pageable pageable) {  
		
		try {
           List<Repayment> repayments=loanService.getRepaymentByloanId(pageable,loanId);
			return ResponseEntity.ok(repayments);
			
        } catch (EntityNotFoundException e) {
			throw new ResourceNotFoundException("Not Found: " + e.getMessage());
        }
	}
	
	 @ExceptionHandler(MethodArgumentTypeMismatchException.class)
    public ResponseEntity<String> handleMethodArgumentTypeMismatchException(MethodArgumentTypeMismatchException ex) {
        String message = String.format("Invalid ID format: '%s'. ID must be an integer.", ex.getValue());
        return ResponseEntity.badRequest().body(message);
    }
	
	 

}

