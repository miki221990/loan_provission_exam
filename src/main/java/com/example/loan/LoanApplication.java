package com.example.loan;
import org.modelmapper.ModelMapper;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.core.env.ConfigurableEnvironment;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@SpringBootApplication
public class LoanApplication {
 private static final Logger logger = LoggerFactory.getLogger(LoanApplication.class);
	public static void main(String[] args) {
		ConfigurableApplicationContext context = SpringApplication.run(LoanApplication.class, args);
        ConfigurableEnvironment env = context.getEnvironment();
        String activeProfiles = String.join(", ", env.getActiveProfiles());
        logger.info("Application started with active profiles: {}", activeProfiles);

        String serverPort = env.getProperty("server.port");
        logger.info("Server is running on port: {}", serverPort);

        if (env.acceptsProfiles("dev")) {
            logger.info("Running in development mode");
        } else if (env.acceptsProfiles("prod")) {
            logger.info("Running in production mode");
        }
	}
	
	@Bean
    public ModelMapper getModelMapper() {
        return new ModelMapper();
    }

}
