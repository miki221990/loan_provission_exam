package com.example.loan.entity;
import jakarta.persistence.*;
import java.sql.Timestamp;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Data
@Entity
@Table(name = "loan")
@AllArgsConstructor
@Builder
@NoArgsConstructor
@ToString
@Getter
@Setter
public class Loan {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(nullable = false, length = 255)
    private Double amount;

    @ManyToOne
    @JoinColumn(name = "customer_id", nullable = false)
    private Customer customer;

    @Column(length = 255)
    private String purpose;

    @Column(length = 255)
    private Double term;

    @Column(length = 255)
    private Double interest;

    @Column(length = 255)
    private Double duration;
    
    @ManyToOne
    @JoinColumn(name="status_id",referencedColumnName = "id")
    private DictionaryItem status;
   

    @Column(nullable = false)
    private Timestamp created_at;

}
