package com.example.loan.entity;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.sql.Timestamp;

@Data
@Entity
@Table(name = "repayment")
@AllArgsConstructor
@Builder
@NoArgsConstructor
@ToString
@Getter
@Setter
public class Repayment {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "loan_id", nullable = false)
    private Loan loan;

    @OneToOne
    @JoinColumn(name = "transaction_id", unique = true)
    private Transaction transaction;

    @Column(nullable = false)
    private Double amountPaid;

    @Column(nullable = false)
    private Double outstandingAmount;

    @Column(nullable = false)
    private Double interestPaid;

    @ManyToOne
    @JoinColumn(name="status_id",referencedColumnName = "id")
    private DictionaryItem status;

    @Column(nullable = false)
    private Timestamp created_at;

}