package com.example.loan.entity;

import jakarta.persistence.*;
import java.sql.Timestamp;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;


@Data
@AllArgsConstructor
@Builder
@NoArgsConstructor
@Entity
@ToString
@Table(name="customer")
@Getter
@Setter
public class Customer {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(nullable = false, length = 255)
    private String username;

    @Column(length = 255)
    private Double salary;

    @Column(length = 255)
    private String accountNo;

    @Column(length = 255)
    private String phoneNumber;

    @Column(nullable = false)
    private Timestamp created_at;

}
