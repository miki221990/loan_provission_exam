CREATE TABLE customer (
  id INT PRIMARY KEY AUTO_INCREMENT,
  username VARCHAR(255),
  salary DOUBLE,
  account_no VARCHAR(255),
  phone_number VARCHAR(255),
  created_at TIMESTAMP
);
CREATE TABLE dictionary (
  id INT PRIMARY KEY AUTO_INCREMENT,
  name VARCHAR(255)
);

CREATE TABLE dictionaryItem (
  id INT PRIMARY KEY AUTO_INCREMENT,
  name VARCHAR(255),
  dictionary_id INT,
  FOREIGN KEY (dictionary_id) REFERENCES dictionary(id)
);

CREATE TABLE loan (
  id INT PRIMARY KEY AUTO_INCREMENT,
  amount DOUBLE,
  customer_id INT,
  purpose VARCHAR(255),
  term DOUBLE,
  interest DOUBLE,
  duration DOUBLE,
  status_id INT,
  created_at TIMESTAMP,
  FOREIGN KEY (customer_id) REFERENCES customer(id),
  FOREIGN KEY (status_id) REFERENCES dictionaryItem(id)
);

CREATE TABLE transaction (
  id INT PRIMARY KEY AUTO_INCREMENT,
  amount DOUBLE,
  account_number VARCHAR(255),
  loan_id INT,
  reason VARCHAR(255),
  status_id INT,
  created_at TIMESTAMP,
  FOREIGN KEY (loan_id) REFERENCES loan(id),
  FOREIGN KEY (status_id) REFERENCES dictionaryItem(id)
);

CREATE TABLE repayment (
  id INT PRIMARY KEY AUTO_INCREMENT,
  loan_id INT,
  amount_paid DOUBLE,
  interest_paid DOUBLE,
  outstanding_amount DOUBLE,
  transaction_id INT UNIQUE,
  status_id INT,
  created_at TIMESTAMP,
  FOREIGN KEY (loan_id) REFERENCES loan(id),
  FOREIGN KEY (transaction_id) REFERENCES transaction(id),
  FOREIGN KEY (status_id) REFERENCES dictionaryItem(id)

);



CREATE UNIQUE INDEX idx_transaction_id ON disposition(transaction_id);
CREATE UNIQUE INDEX idx_transaction_id_rep ON repayment(transaction_id);
